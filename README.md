# Data Processing with Vector Database Using Manhattan Distance

The Qdrant Data Management initiative is designed to enhance data handling and retrieval through the Qdrant vector search engine. This project includes features for adding, searching, visualizing, and summarizing data within Qdrant collections. It involves the use of Docker commands to initiate a Qdrant container, configuring it to map specific ports, including setting up the gRPC port. This project implement a vector database that takes in vector containing values and subject name and return results after some filters.

## Goals
* Ingest data into Vector database
* Perform queries and aggregations
* Visualize output

## Steps
### Step 1: Initialize Vector DB
* Download Vector DB Qdrant
```
docker pull qdrant/qdrant
```
* Run Qdrant
```
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```
### Step 2: Initialize Vector DB
* Set up required dependencies in `Cargo.toml`.
* Set up the functionalities in `main.rs`.
1. Create a collection
 ```
async fn create_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let _ = client.delete_collection(collection_name).await;
    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Manhattan.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}
 ```
 2. Create point with payload
  ```
    async fn ingest_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Create 10 vectors
    let points = vec![
        PointStruct::new(
            1,
            vec![0.06, 0.58, 0.75, 0.83],
            json!({"subject": "Mathematics"}).try_into().unwrap(),
        ),
    ........
    PointStruct::new(
            10,
            vec![0.56, 0.73, 0.94, 0.27],
            json!({"subject": "Physics"}).try_into().unwrap(),
        ),
    ];

    client.upsert_points_blocking(collection_name, None, points, None).await.unwrap();
    Ok(())
}
  ```
3. Data Querying & Filtering

## Step 3: Run DB and Check Results
* cd to the directory and run
```
cargo run
```

# Results and Visualization
![docker](https://gitlab.com/dukeaiml/IDS721/vector_database/-/wikis/uploads/6508fdfa9f18044f4d3bfd1613c44514/docker.png)
![results](https://gitlab.com/dukeaiml/IDS721/vector_database/-/wikis/uploads/c823be3165be38232dffa4c69ecad292/results.png)



